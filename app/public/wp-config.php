<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't/mkjugZve8xC0zfLFAOFMWhakZtdPpqdcL5TuR9CxKKMzqR3Tj3HegsHSX1AFhEOUg287FfZLQXY0m8UvtYRA==');
define('SECURE_AUTH_KEY',  '+OFjMV5k98kHuPm2D6IgiVFF+X+SAWkigEzpY/86AdA2x0S7g60Ttd3rLxIY7X5/zrifoelMYyDrF+DSpDmgBw==');
define('LOGGED_IN_KEY',    'lL1zQZvPTqSLDmjG60A/TsFy97KvbPYkptpUVT50vfGghJymZlXLEb089uL9zKyfnpbytEyuLvXH0JUKcKM1sg==');
define('NONCE_KEY',        '1twS+uJn2kSyjYGZjsF/oH3vEhU0j/tEWbHGcgH24aV3oDUBXoYV+lyJGpoK3wM2a1gYuiLq+kC13a1f+QjqzA==');
define('AUTH_SALT',        'K5bbM+A6jX1GHPE7Ydd+aC+6rYkPoqVIsPg7AJbyOForGCJ6QRy2Ih6lMfqc0nBX68Rda5JqKx8rZ73BOzCGXQ==');
define('SECURE_AUTH_SALT', 'W/xHjhteFumTHG9qxQWKmo5xPSTK56vPDVZK23mp1eZrrc7UDjnRCaWF2oTcbPGIZZPbaLJLHuUdH3dz6I1nvg==');
define('LOGGED_IN_SALT',   'p6kHDk+pQR9uSNIejgPumsnT87EOFuvdLlvEOpYH0n537vkVSXJsXB837RLZ47JmDxEYgUCnKE+TnAS4xEoLLA==');
define('NONCE_SALT',       'TNcEceNcMn2Vew14weB+BK7VcrEKrb5i2ewC0ZXwN51RMXGPhaJkYSnqdhkJzvzgdyyFAfe3kkL9RgRriAZ07A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
